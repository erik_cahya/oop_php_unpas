<?php
class Produk
{
    public $judul,
        $penulis,
        $penerbit,
        $harga,
        $id;

    public function __construct($id, $judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
    {
        $this->id = $id;
        $this->judul = $judul;
        $this->penerbit = $penerbit;
        $this->penulis = $penulis;
        $this->harga = $harga;
    }

    public function addLabel()
    {
        // Jika id = 1, dia akan menambahkan string judul komik
        // Jika id = 2, dia akan menambahan string judul game

        $idProduk = $this->id;

        if ($idProduk == 1) {
            $keterangan = "Judul Komik : " . $this->judul . " | Ditulis oleh : " . $this->penulis . " | Diterbitkan oleh : " . $this->penerbit . " | Dengan harga : " . $this->harga;
        } else if ($idProduk == 2) {
            $keterangan = "Judul Game : " . $this->judul . " | Ditulis oleh : " . $this->penulis . " | Diterbitkan oleh : " . $this->penerbit . " | Dengan harga : " . $this->harga;
        } else {
            $keterangan = "Tidak ada komik,game yang cocok";
        }

        return $keterangan;
    }
}

class cetakInfoProduk
{
    // Produk : diisi dengan nama class yang akan di panggil (dan inilah inti dari object type)
    public function cetakInfo(Produk $produk)
    {
        $str = "{$produk->judul} | {$produk->addLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// instansiasi class
$produk01 = new Produk(1, "Naruto", "Mashashi Kishimoto", "Shonen Jump", 80000);
$produk02 = new Produk(2, "Call of Duty Modern Warfare", "Michael Schiffer", "Activision", 1000000);
$produk03 = new Produk(2, "Paladins", "Orange Dev", "STEAM", "Free to Play");

echo $produk01->addLabel();
echo '<hr>';
echo $produk02->addLabel();
echo '<hr>';
echo $produk03->addLabel();
echo '<hr>';

$infoProduk01 = new cetakInfoProduk();
echo $infoProduk01->cetakInfo($produk01);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Object Type</title>
</head>

<body>

</body>

</html>