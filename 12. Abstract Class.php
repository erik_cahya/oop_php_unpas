<?php
// Abstract Class
// 1. Sebuah class yang tidak dapat di instansiasi
// 2. Kelas Abstrak
// 3. Mendefinisikan interface(method) untuk kelas lain yang menjadi turunannya
// 4. Berperan sebagai 'kerangka dasar' untuk kelas turunannya 
// 5. Memiliki minimal 1 method abstrak
// 6. Digunakan dalam 'pewarisan' / inheritance untuk memaksakan implementasi method abstrak yang sama untuk semua kelas turunannya
// sehingga di parent, memiliki method yang isinya kosong, dan kita mengisikan method tersebut di child/turunannya yang memiliki nama method yang sama

class Produk
{
    private $judul,
        $penulis,
        $penerbit,
        $diskon = 0;

    private $harga;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
    {
        $this->judul = $judul;
        $this->penerbit = $penerbit;
        $this->penulis = $penulis;
        $this->harga = $harga;
    }

    public function getLabel()
    {
        return "$this->penulis, $this->penerbit";
    }







    // --------------------------- Contoh Setter ------------------------

    // Ini adalah contoh method setter (yaitu merubah judul dari property yang di private & nilainya harus ber-tipe stirng)
    public function setJudul($judul)
    {
        // if (!is_string($judul)) : untuk menampilkan pesan error ketika meninput selain tipe string
        if (!is_string($judul)) {
            throw new Exception("Judul Harus bernilai string");
        }

        $this->judul = $judul;
    }
    public function setPenulis($penulis)
    {
        $this->penulis = $penulis;
    }

    public function setPenerbit($penerbit)
    {
        $this->penerbit = $penerbit;
    }

    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }
    // --------------------------- Akhir Setter ------------------------


    // --------------------------- Contoh Getter ------------------------
    // Ini adalah contoh method getter (yaitu meng-GET sebuah judul dari property yang di private)
    public function getJudul()
    {

        return $this->judul;
    }
    public function getPenulis()
    {
        return $this->penulis;
    }
    public function getPenerbit()
    {
        return $this->penerbit;
    }

    public function getDiskon()
    {
        return $this->diskon;
    }
    // --------------------------- Akhir Getter ------------------------








    public function getInfoProduk()
    {
        // Komik : Naruto | Mashashi Kishimoto, Shonen Jump (Rp. 80000) - 100 Halaman

        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

    public function getHarga()
    {
        // set diskon
        return $this->harga - ($this->harga * $this->diskon / 100);
    }
}

// Syntax Inheritance (extends)
class Komik extends Produk
{
    public $jumlahHalaman;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jumlahHalaman = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);

        $this->jumlahHalaman = $jumlahHalaman;
    }

    public function getInfoProduk()
    {
        return "Komik : " . parent::getInfoProduk() . " - {$this->jumlahHalaman} Halaman";
    }
    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }
}

class Game extends Produk
{
    public $waktuMain;
    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $waktuMain = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->waktuMain;
    }
    public function getInfoProduk()
    {
        return "Game : " . parent::getInfoProduk() . " - {$this->waktuMain} Jam";
    }
}


class cetakInfoProduk
{
    public $daftarProduk = array();

    public function tambahProduk(Produk $produk)
    {
        $this->daftarProduk[] = $produk;
    }

    // Produk : diisi dengan nama class yang akan di panggil (dan inilah inti dari object type)
    public function cetakInfo()
    {
        $str = "DAFTAR PRODUK : <br>";

        foreach ($this->daftarProduk as $prod) {
            $str .= "- {$prod->getInfoProduk()} <br>";
        }
        return $str;
    }
}

// instansiasi class
$produk01 = new Komik("Naruto", "Mashashi Kishimoto", "Shonen Jump", 80000, 100);
$produk02 = new Game("Call of Duty Modern Warfare", "Michael Schiffer", "Activision", 1000000, 0, 50);

$cetakProduk = new cetakInfoProduk();
$cetakProduk->tambahProduk($produk01);
$cetakProduk->tambahProduk($produk02);
echo $cetakProduk->cetakInfo();


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Object Type</title>
</head>

<body>

</body>

</html>