<?php
// Setter dan getter : memungkinkan kita untuk melakukan validasi

class Produk
{
    private $judul,
        $penulis,
        $penerbit,
        $diskon = 0;

    private $harga;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
    {
        $this->judul = $judul;
        $this->penerbit = $penerbit;
        $this->penulis = $penulis;
        $this->harga = $harga;
    }

    public function getLabel()
    {
        return "$this->penulis, $this->penerbit";
    }







    // --------------------------- Contoh Setter ------------------------

    // Ini adalah contoh method setter (yaitu merubah judul dari property yang di private & nilainya harus ber-tipe stirng)
    public function setJudul($judul)
    {
        // if (!is_string($judul)) : untuk menampilkan pesan error ketika meninput selain tipe string
        if (!is_string($judul)) {
            throw new Exception("Judul Harus bernilai string");
        }

        $this->judul = $judul;
    }
    public function setPenulis($penulis)
    {
        $this->penulis = $penulis;
    }

    public function setPenerbit($penerbit)
    {
        $this->penerbit = $penerbit;
    }

    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }
    // --------------------------- Akhir Setter ------------------------


    // --------------------------- Contoh Getter ------------------------
    // Ini adalah contoh method getter (yaitu meng-GET sebuah judul dari property yang di private)
    public function getJudul()
    {

        return $this->judul;
    }
    public function getPenulis()
    {
        return $this->penulis;
    }
    public function getPenerbit()
    {
        return $this->penerbit;
    }

    public function getDiskon()
    {
        return $this->diskon;
    }
    // --------------------------- Akhir Getter ------------------------








    public function getInfoProduk()
    {
        // Komik : Naruto | Mashashi Kishimoto, Shonen Jump (Rp. 80000) - 100 Halaman

        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

    public function getHarga()
    {
        // set diskon
        return $this->harga - ($this->harga * $this->diskon / 100);
    }
}

// Syntax Inheritance (extends)
class Komik extends Produk
{
    public $jumlahHalaman;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jumlahHalaman = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);

        $this->jumlahHalaman = $jumlahHalaman;
    }

    public function getInfoProduk()
    {
        return "Komik : " . parent::getInfoProduk() . " - {$this->jumlahHalaman} Halaman";
    }
    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }
}

class Game extends Produk
{
    public $waktuMain;
    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $waktuMain = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->waktuMain;
    }
    public function getInfoProduk()
    {
        return "Game : " . parent::getInfoProduk() . " - {$this->waktuMain} Jam";
    }
}



class cetakInfoProduk
{
    // Produk : diisi dengan nama class yang akan di panggil (dan inilah inti dari object type)
    public function cetakInfo(Produk $produk)
    {
        $str = "{$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// instansiasi class
$produk01 = new Komik("Naruto", "Mashashi Kishimoto", "Shonen Jump", 80000, 100);
$produk02 = new Game("Call of Duty Modern Warfare", "Michael Schiffer", "Activision", 1000000, 0, 50);



// echo $produk01->getLabel();
// echo '<hr>';
echo $produk01->getInfoProduk();
echo '<br>';
echo $produk02->getInfoProduk();
echo '<hr>';
$produk02->setDiskon(12);
echo $produk02->getHarga();
echo "<hr>";

$produk01->setPenerbit("JudulBaru");
echo $produk01->getPenerbit();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Object Type</title>
</head>

<body>

</body>

</html>