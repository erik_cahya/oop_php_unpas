<!-- 
** Property & Method 

-- Property --- 
1. Berfungsi untuk merepresentasikan data/keadaan dari sebuah object
2. Variabel yang ada di dalam object ( member variable )
3. Aturannya sama seperti variable di dalam PHP, ditambah dengan visibility (private/public/protected) di depannya 

-- Method --
1. Merepresentasikan perilaku/behavior dari sebuah object
2. Function yang ada di dalam object
3. Aturannya sama seperti function di dalam PHP, ditambah dengan visibility (private/public/protected) di depannya 

-->




<?php
class Produk
{

    public  $judul = "Bleach",   //Property //"Bleach" adalah nilai default
        $penulis,   //Property
        $penerbit;  //Property


    // Method
    public function addLabel()
    {
        return "Komik ini berjudul : $this->judul yang ditulis oleh $this->penulis dan diterbitkan oleh $this->penerbit";
    }
}

$produk3 = new Produk();
$produk3->judul = "Naruto";
$produk3->penulis = "Mashashi Kishimoto";
$produk3->penerbit = "Shonen Jump";
$produk3->harga = 30000; //Kita bisa menambah property baru tanpa harus menambahkannya di dalam class

// echo "Komik ini berjudul : " . $produk3->judul . " yang ditulis oleh " . $produk3->penulis;
echo $produk3->addLabel();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Property & Method</title>
</head>

<body>

</body>

</html>