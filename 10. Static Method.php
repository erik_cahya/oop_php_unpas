<?php
// Kegunaan static keyword
// 1. Member(property & method) yang terikat dengan class, bukan dengan object
// 2. Nilai static akan selalu tetap meskipun object di-instansiasi berulang kali 
// 3. Membuat kode menjadi procedural
// 4. Biasanya digunakan untuk membuat fungsi bantuan / helper
// 5. Atau biasanya digunakan juga untuk class class utility pada Framework


// Contoh penulisan static
class contohStatic
{
    // contoh penulisan property static
    public static $angka = 1;
    public static $string = "Ini Stirng";

    // Contoh penulisan method static
    public static function helloWorld()
    {
        return "Hello World";
    }

    public static function halo()
    {
        // self::angka : digunakan untuk menggantikan $this->angka pada static method
        return "Halo " . self::$angka . " Kali";
    }
}

// Cara mengakses / intansiasi property & method static
// dengan cara tulis langsung nama classnya kemudian titik 2, 2kali, lalu nama property-nya 
// (namaClass::variable-Property)

echo contohStatic::$string;
echo "<br>";
echo contohStatic::helloWorld();
echo "<hr>";
echo contohStatic::halo();



?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Static Method</title>
</head>

<body>

</body>

</html>