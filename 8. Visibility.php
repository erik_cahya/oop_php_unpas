<?php
// Ada 3 Keyword Visibilty :
// 1. public : dapat digunakan di mana saja, bahkan diluar class
// 2. protected : hanya dapat digunakan di dalam sebuah kelas beserta turunannya (inheritance)
// 3. private : hanya dapat digunakan di dalam sebuah class tertentu saja / hanya class yang di set

class Produk
{
    public $judul,
        $penulis,
        $penerbit;

    private $harga;

    protected $diskon = 0;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0)
    {
        $this->judul = $judul;
        $this->penerbit = $penerbit;
        $this->penulis = $penulis;
        $this->harga = $harga;
    }

    public function getLabel()
    {
        return "$this->penulis, $this->penerbit";
    }

    public function getInfoProduk()
    {
        // Komik : Naruto | Mashashi Kishimoto, Shonen Jump (Rp. 80000) - 100 Halaman

        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }

    public function getHarga()
    {
        // set diskon
        return $this->harga - ($this->harga * $this->diskon / 100);
    }
}

// Syntax Inheritance (extends)
class Komik extends Produk
{
    public $jumlahHalaman;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jumlahHalaman = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);

        $this->jumlahHalaman = $jumlahHalaman;
    }

    public function getInfoProduk()
    {
        return "Komik : " . parent::getInfoProduk() . " - {$this->jumlahHalaman} Halaman";
    }
    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }
}

class Game extends Produk
{
    public $waktuMain;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $waktuMain = 0)
    {
        parent::__construct($judul, $penulis, $penerbit, $harga);
        $this->waktuMain;
    }

    public function setDiskon($diskon)
    {
        return $this->diskon = $diskon;
    }

    public function getInfoProduk()
    {
        return "Game : " . parent::getInfoProduk() . " - {$this->waktuMain} Jam";
    }
}



class cetakInfoProduk
{
    // Produk : diisi dengan nama class yang akan di panggil (dan inilah inti dari object type)
    public function cetakInfo(Produk $produk)
    {
        $str = "{$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// instansiasi class
$produk01 = new Komik("Naruto", "Mashashi Kishimoto", "Shonen Jump", 80000, 100);
$produk02 = new Game("Call of Duty Modern Warfare", "Michael Schiffer", "Activision", 1000000, 0, 50);



// echo $produk01->getLabel();
// echo '<hr>';
echo $produk01->getInfoProduk();
echo '<br>';
echo $produk02->getInfoProduk();
echo '<hr>';
$produk02->setDiskon(90);
echo $produk02->getHarga();

// $infoProduk01 = new cetakInfoProduk();
// echo $infoProduk01->cetakInfo($produk01);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Object Type</title>
</head>

<body>

</body>

</html>