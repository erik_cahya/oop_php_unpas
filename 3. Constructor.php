<!-- __construct Method -->
<!-- Adalah sebuah method yang akan langsung dijalankan ketika kita membuat insransiasi dari sebuah class -->

<?php
class Produk
{
    public $judul,
        $penulis,
        $penerbit;

    public function __construct($judul, $penulis, $penerbit)
    {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
    }
}


$produk04 = new Produk("Naruto", "Mashashi Kishimoto", "Shonen Jump");
var_dump($produk04->judul);
echo "<br>";
echo $produk04->penerbit;


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Constructor Method</title>
</head>

<body>

</body>

</html>