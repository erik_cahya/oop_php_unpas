<?php
// Constant : sebuah indetifier untuk menyimpan nilai
// Bedanya dengan variable adalah constant nilainya tidak dapat berubah hingga program selesai dijalankan

// Ada 2 cara untuk membuat constant ini : 
// 1. define()
// 2. const

// Perbedaan antara const & define
// Jika define() : dia tidak bisa dimasukkan ke dalam class. Sehingga harus ditulis paling atas sebagai object global
// Jika const : kita bisa memasukkannya ke dalam class, sehingga kita bisa menggunakannya di dalam konsep OOP programming

// ada 2 parameter : 
// 1. Nama Konstantanya apa 
// 2. Isi dari konstantanya
define('NAMA_SAYA', 'Erik Cahya Pradana');
// cara mengaksesnya sama seperti variable biasa, cuma tanpa tanda dolar
echo NAMA_SAYA;
echo "<br>";
const UMUR = 19;
echo UMUR;
echo "<hr>";


// ------ Cara membuat dan akses konstanta dengan class

class contohConst
{
    const NAMA = "Erik Cahya Pradana";
}

// mengaksesnya sama seperti static method
// dengan cara tulis langsung nama classnya kemudian titik 2, 2kali, lalu nama konstanta-nya 
// (namaClass::variable-Property)
echo contohConst::NAMA;





// ---------------------------------------------------------------------
// Magic Constant PHP
// 1. __LINE__ : untuk menunjukkan ada di baris berapa kodingan LINE tersebut
// 2. __FILE__ : menunjukkan lokasi penyimpanan file codingan kita
// 3. __DIR__ : menunjukkan direktori/folder codingan kita
// 4. __FUNCTION__ : menunjukkan code tertentu sedang berada di function apa
// 5. __CLASS__ : menunjukkan code tertentu sedang berada di class apa

// __LINE__
echo "<br><br>" . __LINE__;

// __FILE__
echo "<br>" . __FILE__;

// __DIR__
echo "<br>" . __DIR__;

// __FUNCTION__
function coba()
{
    return __FUNCTION__;
}
echo "<br>" . coba();

// __CLASS__
class cobaConst
{
    public static $class = __CLASS__;
}
echo "<br>" . cobaConst::$class;
