<!-- 
-- Inheritance --
Adalah konsep untuk menciptakan hierarki antar kelas (parent & child)

* Child class, mewarisi semua properti dan method dari parentn-nya (dengan syarat : yang visible)
* Child class, memperluas (extends) fungsionalitas dari parent-nya
 -->
<?php
class Produk
{
    public $judul,
        $penulis,
        $penerbit,
        $harga,
        $jmlHalaman,
        $waktuMain;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0, $jmlHalaman = 0, $waktuMain = 0)
    {
        $this->judul = $judul;
        $this->penerbit = $penerbit;
        $this->penulis = $penulis;
        $this->harga = $harga;
        $this->jmlHalaman = $jmlHalaman;
        $this->waktuMain = $waktuMain;
    }

    public function getLabel()
    {
        return "$this->penulis, $this->penerbit";
    }

    public function getInfoProduk()
    {
        // Komik : Naruto | Mashashi Kishimoto, Shonen Jump (Rp. 80000) - 100 Halaman

        $str = "{$this->judul} | {$this->getLabel()} (Rp. {$this->harga})";
        return $str;
    }
}

// Syntax Inheritance (extends)
class Komik extends Produk
{
    public function getInfoProduk()
    {
        return "Komik : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga}) - {$this->jmlHalaman} Halaman";
    }
}

class Game extends Produk
{
    public function getInfoProduk()
    {
        return "Game : {$this->judul} | {$this->getLabel()} (Rp. {$this->harga}) - {$this->waktuMain} Jam";
    }
}



class cetakInfoProduk
{
    // Produk : diisi dengan nama class yang akan di panggil (dan inilah inti dari object type)
    public function cetakInfo(Produk $produk)
    {
        $str = "{$produk->getLabel()} (Rp. {$produk->harga})";
        return $str;
    }
}

// instansiasi class
$produk01 = new Komik("Naruto", "Mashashi Kishimoto", "Shonen Jump", 80000, 100, 0);
$produk02 = new Game("Call of Duty Modern Warfare", "Michael Schiffer", "Activision", 1000000, 0, 50);



// echo $produk01->getLabel();
// echo '<hr>';
echo $produk02->getLabel();
echo '<hr>';
echo $produk01->getInfoProduk();
echo '<hr>';
echo $produk02->getInfoProduk();
echo '<hr>';

// $infoProduk01 = new cetakInfoProduk();
// echo $infoProduk01->cetakInfo($produk01);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Object Type</title>
</head>

<body>

</body>

</html>